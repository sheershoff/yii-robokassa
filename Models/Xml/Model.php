<?php
/**
 * \Robokassa\Models\Xml\Model class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @link https://github.com/yiiext
 * @version 0.1
 */

namespace Robokassa\Models\Xml;

use SimpleXMLIterator,
	Traversable,
	IteratorAggregate,
	Yii,
	CLogger,
	CException as Exception,
	CComponent as Component;

/**
 * XML-интерфейсы
 *
 * TODO:
 * Для работы компонента требуется XML-parser.
 * Check if components after init
 *
 * @property SimpleXMLIterator $xml
 * @property SimpleXMLIterator|Traversable $iterator
 *
 * @link http://robokassa.ru/
 * @see http://robokassa.ru/ru/Doc/Ru/Interface.aspx
 */
abstract class Model extends Component implements IteratorAggregate
{
	public static $baseUrl = 'https://merchant.roboxchange.com/WebService/Service.asmx';

	/** @var SimpleXMLIterator */
	private $_xml;

	/**
	 * @return string
	 */
	abstract public function methodName();

	/**
	 * @return array
	 */
	abstract public function params();

	/**
	 *
	 */
	public function loadXml()
	{
		$this->_xml = $this->requestData($this->methodName(), $this->params());
	}

	/**
	 * @return SimpleXMLIterator
	 */
	public function getXml()
	{
		if (null === $this->_xml) {
			$this->loadXml();
		}
		return $this->_xml;
	}

	/**
	 * Retrieve an external iterator.
	 * @return SimpleXMLIterator|Traversable
	 */
	public function getIterator()
	{
		return $this->getXml();
	}

	/**
	 * TODO: Create event.
	 * @param string $error
	 */
	protected function onRequestError($error)
	{
		Yii::log($error, CLogger::LEVEL_ERROR);
	}

	/**
	 * @param string $method
	 * @param array $params
	 * @return SimpleXMLIterator
	 */
	protected function requestData($method, array $params)
	{
		// Prepare request url.
		$url = self::$baseUrl . '/' .$method . '?';
		foreach ($params as $key => $value) {
			$url .= $key . '=' . $value . '&';
		}

		$requestOptions = array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			// Use POST request.
			//CURLOPT_POST => count($params),
			//CURLOPT_POSTFIELDS => $params,
		);

		try {
			$ch = curl_init();
			curl_setopt_array($ch, $requestOptions);
			$response = curl_exec($ch);
			curl_close($ch);

			if (false === $response) {
				$this->onRequestError('Curl error: ' . curl_error($ch));
				return false;
			}

			libxml_use_internal_errors(true);
			$response = new SimpleXMLIterator($response);

			$className = get_class($this);
			$className = substr($className, strrpos($className, '\\') + 1);
			if ($response->getName() !== $className) {
				$this->onRequestError('Invalid response: wanted ' . $className . ' got ' . $response->getName());
				return false;
			}
		} catch (Exception $e) {
			$this->onRequestError('Cannot execute curl action');
			return false;
		}

		return $response;
	}
}