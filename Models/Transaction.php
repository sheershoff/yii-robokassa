<?php
/**
 * \Robokassa\Models\Transaction class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @link https://github.com/yiiext
 * @version 0.1
 */

namespace Robokassa\Models;

use Yii,
	CException as Exception,
	YiiPlus\Db\Model,
	Robokassa\Components\RoboKassa,
	Robokassa\Interfaces\Invoice;

/**
 * Payment transaction forms.
 *
 * @property Robokassa $robokassa
 * @property Invoice $invoice
 * @property array $params
 */
class Transaction extends Model
{
	const SCENARIO_CREATE	= 'create';
	const SCENARIO_RESULT	= 'result';
	const SCENARIO_SUCCESS	= 'success';
	const SCENARIO_FAILURE	= 'failure';

	/** @var integer From 1 to 2147483647 (2^31-1). */
	public $invoiceId = 0;
	/** @var float Delimiter dotted. */
	public $amount = 0.00;
	/** @var string */
	public $signature = '';
	/** @var string Max 100 symbols */
	public $description = '';
	/** @var string */
	public $mail = '';
	/** @var string */
	public $currency = '';
	/** @var string */
	public $language = 'en';
	/** @var string */
	public $charset = '';

	/** @var Robokassa */
	private $_robokassa;
	/** @var Invoice */
	private $_invoice;
	/** @var array */
	private $_params = array();

	/**
	 * @param string $scenario
	 * @param Robokassa $robokassa
	 */
	public function __construct($scenario, Robokassa $robokassa)
	{
		parent::__construct($scenario);
		$this->robokassa = $robokassa;
	}

	/**
	 * @param string $name
	 * @return mixed
	 * @throws Exception
	 */
	public function __get($name)
	{
		if (array_key_exists($name, $this->_params)) {
			return $this->_params[$name];
		} else {
			return parent::__get($name);
		}
	}

	/**
	 * @param string $name
	 * @param mixed $value
	 * @return void
	 * @throws Exception
	 */
	public function __set($name, $value)
	{
		if (array_key_exists($name, $this->_params)) {
			$this->_params[$name] = $value;
		} else {
			parent::__set($name, $value);
		}
	}

	/**
	 * @param string $scenario
	 * @throws Exception
	 */
	public function setScenario($scenario)
	{
		if ($scenario !== self::SCENARIO_CREATE &&
			$scenario !== self::SCENARIO_RESULT &&
			$scenario !== self::SCENARIO_SUCCESS &&
			$scenario !== self::SCENARIO_FAILURE) {
			throw new Exception('Invalid transaction scenario.');
		}
		parent::setScenario($scenario);
	}

	/**
	 * @return Robokassa
	 * @throws Exception
	 */
	public function getRobokassa()
	{
		if (null === $this->_robokassa) {
			throw new Exception('Robokassa is not set.');
		}
		return $this->_robokassa;
	}

	/**
	 * @param Robokassa $robokassa
	 */
	public function setRobokassa(Robokassa $robokassa = null)
	{
		$this->_params = array();
		foreach ($robokassa->params as $param) {
			$this->_params[$param] = null;
		}
		$this->_robokassa = $robokassa;
	}

	/**
	 * @return Invoice
	 * @throws Exception
	 */
	public function getInvoice()
	{
		return $this->_invoice;
	}

	/**
	 * @param Invoice $invoice
	 */
	public function setInvoice(Invoice $invoice = null)
	{
		$this->_invoice = $invoice;
	}

	/**
	 * @return array
	 */
	public function getParams()
	{
		return $this->_params;
	}

	/**
	 * @return array
	 */
	public function getLogin()
	{
		return $this->robokassa->merchantLogin;
	}

	/**
	 * @return array
	 */
	public function attributeNames()
	{
		$names = array(
			'login',
			'invoiceId',
			'amount',
			'signature',
			'description',
			'mail',
			'currency',
			'language',
			'charset',
		);
		if (count($this->params) > 0) {
			$names = array_merge($names, array_keys($this->params));
		}
		return $names;
	}

	/**
	 * @return array
	 */
	public function attributeLabels()
	{
		$values = array(
			'login' => 'MrchLogin',
			'invoiceId' => 'InvId',
			'amount' => 'OutSum',
			'signature' => 'SignatureValue',
			'description' => 'Desc',
			'mail' => 'Email',
			'currency' => 'IncCurrLabel',
			'language' => 'Culture',
			'charset' => 'Encoding',
		);
		if (count($this->params) > 0) {
			$params = array_keys($this->params);
			$values = array_merge($values, array_combine($params, $params));
		}
		return $values;
	}

	/**
	 * @return array
	 */
	public function rules()
	{
		$rules = array(
			// Фильтруем данные
			array('invoiceId', 'filter', 'filter' => 'intval',),
			array('amount', 'filter', 'filter' => 'floatval',),
			array('signature, language, description, mail, currency, charset', 'filter', 'filter' => 'strip_tags',),
			array('signature, language, description, mail, currency, charset', 'filter', 'filter' => 'trim',),
			array('signature', 'filter', 'filter' => 'strtolower',),

			// Обзательные поля
			array('invoiceId, amount, language', 'required',),

			// Номер фактуры должен быть больше нуля
			array('invoiceId', 'numerical', 'min' => 1,),
			// Сумма фактуры должна быть больше нуля
			array('amount', 'numerical', 'min' => 0.01,),
			// Проверка существования фактуры
			array('invoiceId', 'validateInvoiceExists',),
			// Сумма фактуры и сумма транзакции должна быть равна
			array('amount', 'validateAmountEquals',),
		);

		if (self::SCENARIO_RESULT === $this->scenario) {
			// Сверка подписи
			$rules[] = array('signature', 'validateSignature');
		}
		if (self::SCENARIO_SUCCESS === $this->scenario) {
			// Сверка подписи
			$rules[] = array('signature', 'validateSignature');

		}
		if (self::SCENARIO_FAILURE=== $this->scenario) {

		}

		// Все пользовательские параметры обязательны и должны быть отфильтрованы
		foreach ($this->params as $param => $value) {
			$rules[] = array($param, 'filter', 'filter' => 'strip_tags',);
			$rules[] = array($param, 'filter', 'filter' => 'trim',);
			$rules[] = array($param, 'required',);
		}

		return $rules;
	}

	/**
	 * @param $attribute
	 * @param array $params
	 */
	public function validateInvoiceExists($attribute, array $params = array())
	{
		if (null === $this->invoice) {
			$className = $this->robokassa->invoiceClassName;
			$this->invoice = new $className;
		}
		$this->invoice = $this->invoice->findByInvoiceId($this->$attribute);
		if (null === $this->invoice) {
			if (!isset($params['message'])) {
				$params['message'] = Yii::t('payment', 'Invoice #{invoice} not found.',
					array('{invoice}' => $this->$attribute,));
			}
			$this->addError($attribute, $params['message']);
		}
	}

	/**
	 * @param $attribute
	 * @param array $params
	 */
	public function validateAmountEquals($attribute, array $params = array())
	{
		if ($this->invoice->getAmount() !== $this->$attribute) {
			if (!isset($params['message'])) {
				$params['message'] = Yii::t('payment', 'Not equal amount: {var1} != {var2}',
					array('{var1}' => $this->invoice->getAmount(), '{var2}' => $this->$attribute,));
			}
			$this->addError($attribute, $params['message']);
		}
	}

	/**
	 * @param $attribute
	 * @param array $params
	 */
	public function validateSignature($attribute, array $params = array())
	{
		$validSignature = $this->hashTransaction();
		if ($this->$attribute !== $validSignature) {
			if (!isset($params['message'])) {
				$params['message'] = Yii::t('payment', 'Invalid signature "{signature}" got "{got}"',
					array('{signature}' => $validSignature, '{got}' => $this->$attribute,));
			}
			$this->addError($attribute, $params['message']);
		}
	}

	/**
	 * @param array $data
	 */
	public function loadData(array $data)
	{
		foreach ($this->attributeLabels() as $name => $param) {
			if ($this->isAttributeSafe($name) && isset($data[$param])) {
				$this->$name = $data[$param];
			}
		}
	}

	/**
	 * @param Invoice $invoice
	 */
	public function chargeInvoice(Invoice $invoice)
	{
		$this->invoice = $invoice;
		// Bound invoice values.
		$this->invoiceId = $invoice->getInvoiceId();
		$this->amount = $invoice->getAmount();
		$this->description = $invoice->getDescription();
		$this->currency = $invoice->getUserCurrency();
		$this->mail = $invoice->getUserMail();
		// Defaults values.
		$this->language = substr(Yii::app()->language, 0, 2);
		$this->charset = Yii::app()->charset;
		// Bound users params values.
		foreach ($this->params as $name => $value) {
			$this->_params[$name] = $invoice->$name;
		}
		// Sign invoice.
		$this->signature = $this->hashTransaction();
	}

	/**
	 * @return string
	 */
	public function hashTransaction()
	{
		if ($this->scenario === self::SCENARIO_RESULT) {
			$keys = array(
				$this->amount,
				$this->invoiceId,
				$this->robokassa->merchantPass2,
			);
		} elseif ($this->scenario === self::SCENARIO_SUCCESS) {
			$keys = array(
				$this->amount,
				$this->invoiceId,
				$this->robokassa->merchantPass1,
			);
		} else { // $this->scenario === self::SCENARIO_CREATE
			$keys = array(
				$this->robokassa->merchantLogin,
				$this->amount,
				$this->invoiceId,
				$this->robokassa->merchantPass1,
			);
		}

		foreach ($this->params as $name => $value) {
			$keys[] = $name . '=' . $value;
		}

		return md5(implode(':', $keys));
	}
}