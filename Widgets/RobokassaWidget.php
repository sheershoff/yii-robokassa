<?php
/**
 * \Robokassa\Widgets\RobokassaWidget class file.
 *
 * @author Veaceslav Medvedev <slavcopost@gmail.com>
 * @license http://www.opensource.org/licenses/bsd-license.php
 * @link https://github.com/yiiext
 * @version 0.1
 */

namespace Robokassa\Widgets;

use Yii,
	CException as Exception,
	CHtml as Html,
	CWidget as Widget,
	Robokassa\Components\Robokassa,
	Robokassa\Models\Transaction,
	Robokassa\Interfaces\Invoice;

/**
 * @property Robokassa $robokassa
 * @property Transaction $transaction
 */
class RobokassaWidget extends Widget
{
	/** @var string */
	public $componentId = 'robokassa';

	/** @var Invoice */
	public $invoice;

	/**
	 * @throws Exception
	 */
	public function run()
	{
		if (!$this->invoice instanceof Invoice) {
			throw new Exception('Invalid invoice.');
		}

		parent::run();
		$this->renderForm();
	}

	/**
	 * @return Robokassa
	 */
	public function getRobokassa()
	{
		return Yii::app()->getComponent($this->componentId);
	}

	/**
	 * @return Transaction
	 */
	public function getTransaction()
	{
		$transaction = new Transaction(Transaction::SCENARIO_CREATE, $this->robokassa);
		$transaction->chargeInvoice($this->invoice);
		return $transaction;
	}

	/**
	 * Render body.
	 */
	public function renderForm()
	{
		$url = 'https://merchant.roboxchange.com/Handler/MrchSumPreview.ashx?';
		foreach ($this->transaction->attributeLabels() as $name => $key) {
			// TODO: It is only test.
			if ($key === 'MrchLogin') {
				$url .= $key . '=' . urlencode('demo') . '&';
				continue;
			}
			$url .= $key . '=' . urlencode($this->transaction->$name) . '&';
		}
		$url = rtrim($url, '&');
		echo Html::scriptFile($url);
	}
}